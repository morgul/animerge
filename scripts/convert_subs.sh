#!/bin/bash
OIFS="$IFS"
IFS=$'\n'

# Make the output directory
mkdir -p $2

for f in `find "$1" -type f \( -name "*.mkv" -o -name "*.mp4" \)`
do
    echo "??: $f"
    file="$(basename -- $f)"
    echo "Processing $file"
    ffmpeg -y -i $f -map 0 -c:v libx264 -profile:v high -level:v 4.1 -c:a copy -c:s srt $2/$file
done
