// ---------------------------------------------------------------------------------------------------------------------
// Resource Access for messing with video files
// ---------------------------------------------------------------------------------------------------------------------

import { normalize } from 'node:path';
import { exec } from 'node:child_process';

// ---------------------------------------------------------------------------------------------------------------------

// TODO: Look this up
const mkvmergePath = '/opt/homebrew/bin/mkvmerge';

export async function mergeVideos(dubFile, subFile, outFile)
{
    const dubPath = normalize(dubFile);
    const subPath = subFile
        .replace(/(\s+)/g, '\\$1')
        .replace(/\(/g, '\\(')
        .replace(/\)/g, '\\)');
    const outPath = normalize(outFile);
    const args = ` --ui-language en_US --priority lower --output "${ outPath }" --language 0:zxx --display-dimensions 0:1920x1080 --chroma-siting 0:1,2 --language 1:en --sub-charset 2:UTF-8 --language 2:en --track-name "2:On Screen" --default-track-flag 2:yes "(" "${ dubPath }" ")" --no-video --language 1:ja --sub-charset 2:UTF-8 --language 2:en --track-name 2:Full "(" ${ subPath } ")" --track-order 0:0,0:1,1:1,0:2,1:2;`;

    return new Promise((resolve, reject) =>
    {
        exec(`${ mkvmergePath } ${ args }`, (error, stdout, stderr) =>
        {
            if(error)
            {
                reject(error);
            }
            resolve(stdout, stderr);
        });
    });
}

// ---------------------------------------------------------------------------------------------------------------------
