// ---------------------------------------------------------------------------------------------------------------------
// Manager for Video files
// ---------------------------------------------------------------------------------------------------------------------

import { resolve, join, basename } from 'node:path';
import { access, readdir } from 'node:fs/promises';
import bb from 'bluebird';

// Resource Access
import { mergeVideos } from '../resource-access/video.js';

// ---------------------------------------------------------------------------------------------------------------------

export async function merge(dubbed, subbed, output, concurrency)
{
    const dubbedDir = resolve(dubbed);
    const subbedDir = resolve(subbed);
    const outDir = resolve(output);

    const dubbedList = await readdir(dubbedDir);
    await bb.map(dubbedList, async (dubFile) =>
    {
        const fileName = basename(dubFile);
        const dub = join(dubbedDir, fileName);
        const sub = join(subbedDir, fileName);
        const out = join(outDir, fileName);

        const subExists = await access(sub)
            .then(() => true)
            .catch(() => false);

        console.log(`Processing ${ fileName }...`);

        try
        {
            await mergeVideos(dub, subExists ? sub : null, out);
            console.log(`  ...finished ${fileName}.`);
        }
        catch (ex)
        {
            console.error(`  ...error ${fileName}: ${ex.stack}`);
        }
    }, { concurrency });
}

// ---------------------------------------------------------------------------------------------------------------------
