// ---------------------------------------------------------------------------------------------------------------------
// Merge files together
// ---------------------------------------------------------------------------------------------------------------------

import { program } from 'commander';

// Libs
import { merge } from './manager/video.js';

// ---------------------------------------------------------------------------------------------------------------------

program
    .description('merges two mkv files together.')
    .requiredOption('-d, --dubbed <dubbed_dir>', 'The directory to look for dubbed files in.')
    .requiredOption('-s, --subbed <subbed_dir>', 'The directory to look for matching subbed files.')
    .requiredOption('-o, --output <output_dir>', 'The directory to output merged files to.')
    .option('-c, --concurrency <concurrency>', 'The number of concurrent downloads to allow.', '6');

// Parse arguments
program.parse();

// ---------------------------------------------------------------------------------------------------------------------

// Parse concurrency
if(program.opts().concurrency)
{
    program.opts().concurrency = parseInt(program.opts().concurrency);
}

// ---------------------------------------------------------------------------------------------------------------------

const { dubbed, subbed, output, concurrency } = program.opts();

await merge(dubbed, subbed, output, concurrency);

// We're good.
process.exit(0);

// ---------------------------------------------------------------------------------------------------------------------
