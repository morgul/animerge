# AniMerge

This is just a simple utility for merging the audio and subtitles from a subbed anime with those of a dubbed one.

## Usage

```bash
$ node ./src/merge.js --help
Usage: merge [options]

merges two mkv files together.

Options:
  -d, --dubbed <dubbed_dir>        The directory to look for dubbed files in.
  -s, --subbed <subbed_dir>        The directory to look for matching subbed files.
  -o, --output <output_dir>        The directory to output merged files to.
  -c, --concurrency <concurrency>  The number of concurrent downloads to allow. (default: "6")
  -h, --help                       display help for command
```

### Examples

```bash
$ node ./src/merge.js -d Anime/Dubbed/ -s Anime/Subbed/ -o Anime/Output -c 4
```

